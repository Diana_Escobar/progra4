/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import entidades.Persona;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import modelos.personaModel;

/**
 *
 * @author USUARIO
 */
@Named(value = "Bpersona")
public class PersonaBean {

    private personaModel personaModel = new personaModel();
    private Persona persona = new Persona();

    /**
     * Creates a new instance of PersonaBean
     */
    
        
    private List<Persona> listado = new ArrayList<>();

    public List<Persona> getListado() {
        return listado;
    }

    public String agregar() {
        listado.add(this.persona);
        this.persona = new Persona();

        return "index?faces-redirect=true";
    }

    public void editar(int id) {
        //recorrer la lista y verificar el id
        //extraer la persona con el id indicado
        //establecer el objeto this.persona con el objeto extraido de la lista
        //redireccionar a la vista de editar

        System.out.println("Se quiere editar a la persona con id " + id);

        //return "index?faces-redirect=true";
    }
    
    public PersonaBean() {
        //Inicializar objetos
    }
    
    //Getter && Setter de los modelos ó demás objetos que se mostrarán en la vista
    public Persona getPersonaModel() {
        
        return  personaModel.consultar();
    }


    /**
     * @return the persona
     */
    public Persona getPersona() {
        return persona;
    }

    /**
     * @param persona the persona to set
     */
    public void setPersona(Persona persona) {
        this.persona = persona;
    }
}
