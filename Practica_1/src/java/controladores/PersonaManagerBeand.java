/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import entidades.Persona;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import modelos.personaModel;

/**
 *
 * @author USUARIO
 */
@Named(value = "Bpersona")
@SessionScoped
public class PersonaManagerBeand implements Serializable {

   private personaModel personaModel = new personaModel();
   private Persona persona = new Persona();
   
   private List<Persona> listado = new ArrayList<>();

    public List<Persona> getListado() {
        return listado;
    }
    
    public String agregar(){
        listado.add(this.persona);
        this.persona = new Persona();
        
        return "index?faces-redirect=true";
    }
    
    public void editar(int id){
        
    }

    public personaModel getPersonaModel() {
        return personaModel;
    }

    
    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
   
   
    public PersonaManagerBeand() {
        
    }
    
}
